*========================================================================================
* Converts a project into XML and viceversa
*========================================================================================
Lparameters tcProject

	*--------------------------------------------------------------------------------------
	* A few settings
	*--------------------------------------------------------------------------------------
	Set Deleted on
	
	*--------------------------------------------------------------------------------------
	* Query the project file if none has been passed in
	*--------------------------------------------------------------------------------------
	Local lcProject
	If Vartype(m.tcProject) == "C" and File(m.tcProject)
		lcProject = m.tcProject
	Else
		lcProject = GetFile( "twofox;pjx", "Select a project file" )
		If not File(m.lcProject)
			Return
		EndIf 
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Either create a PJX or a twofox file
	*--------------------------------------------------------------------------------------
	Local loMerge, loSplit
	If Upper(JustExt(m.lcProject)) == "TWOFOX"
		loMerge = NewObject("CMergePJX","TwoFox.prg")
		loMerge.Convert( m.lcProject )
	Else
		loSplit = NewObject("CSPlitPJX","TwoFox.prg")
		loSplit.Convert( m.lcProject )
	EndIf
